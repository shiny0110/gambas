#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: gb.eval.highlight 3.16.90\n"
"POT-Creation-Date: 2021-11-05 16:03 UTC\n"
"PO-Revision-Date: 2021-11-05 11:54 UTC\n"
"Last-Translator: Benoît Minisini <g4mba5@gmail.com>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: Highlight.class:27
msgid "Added text"
msgstr "Texte ajouté"

#: Highlight.class:27
msgid "Alternate background"
msgstr "Arrière-plan alternatif"

#: Highlight.class:27
msgid "Attributes"
msgstr "Attributs"

#: Highlight.class:27
msgid "Background"
msgstr "Arrière-plan"

#: Highlight.class:27
msgid "Breakpoints"
msgstr "Points d'arrêt"

#: Highlight.class:27
msgid "CSS pseudo-classes"
msgstr "Pseudo-classes CSS"

#: Highlight.class:27
msgid "CSS rules"
msgstr "Règles CSS"

#: Highlight.class:27
msgid "Class selectors"
msgstr "Sélecteurs de classe"

#: Highlight.class:27
msgid "Comments"
msgstr "Commentaires"

#: Highlight.class:27
msgid "Constants"
msgstr "Constantes"

#: Highlight.class:27
msgid "Current line"
msgstr "Ligne courante"

#: Highlight.class:27
msgid "Datatypes"
msgstr "Types de données"

#: Highlight.class:27
msgid "Documentation"
msgstr "Documentation"

#: Highlight.class:27
msgid "Element selectors"
msgstr "Sélecteurs d'éléments"

#: Highlight.class:27
msgid "Entities"
msgstr "Entités"

#: Highlight.class:27
msgid "Errors"
msgstr "Erreurs"

#: Highlight.class:27
msgid "Escaped characters"
msgstr "Caractères échappés"

#: Highlight.class:27
msgid "Execution line"
msgstr "Ligne d'exécution"

#: Highlight.class:27
msgid "File names"
msgstr "Noms de fichier"

#: Highlight.class:27
msgid "Functions"
msgstr "Fonctions"

#: Highlight.class:27
msgid "Header"
msgstr "Entête"

#: Highlight.class:27
msgid "Highlighting"
msgstr "Mise en évidence"

#: Highlight.class:27
msgid "Id selectors"
msgstr "Sélecteurs d'identifiant"

#: Highlight.class:27
msgid "Important values"
msgstr "Valeurs importantes"

#: Highlight.class:27
msgid "Keywords"
msgstr "Mots-clés"

#: Highlight.class:27
msgid "Labels"
msgstr "Étiquettes"

#: Highlight.class:27
msgid "Markups"
msgstr "Balises"

#: Highlight.class:27
msgid "Normal text"
msgstr "Texte normal"

#: Highlight.class:27
msgid "Numbers"
msgstr "Nombres"

#: Highlight.class:27
msgid "Operators"
msgstr "Opérateurs"

#: Highlight.class:27
msgid "Positions"
msgstr "Positions"

#: Highlight.class:27
msgid "Preprocessor"
msgstr "Préprocesseur"

#: Highlight.class:27
msgid "Properties"
msgstr "Propriétés"

#: Highlight.class:27
msgid "Removed text"
msgstr "Texte supprimé"

#: Highlight.class:27
msgid "Selection background"
msgstr "Arrière-plan de sélection"

#: Highlight.class:27
msgid "Strings"
msgstr "Chaînes"

#: Highlight.class:27
msgid "Symbols"
msgstr "Symboles"

#: Highlight.class:27
msgid "Values"
msgstr "Valeurs"

#: Highlight.class:27
msgid "Webpage comments"
msgstr "Commentaires de Webpage"

#: Highlight.class:27
msgid "Webpage markup arguments"
msgstr "Arguments des balises de Webpage"

#: Highlight.class:27
msgid "Webpage markups"
msgstr "Balises de WebPage"
