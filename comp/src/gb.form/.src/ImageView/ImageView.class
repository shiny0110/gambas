' Gambas class file

Export
Inherits UserControl

Public Const _Properties As String = "*,Border=True,Alignment{Align.*}=Center,Grid,UseMouse=True"
Public Const _DefaultSize As String = "36,36"
Public Const _Group As String = "View"
Public Const _DrawWith As String = "DrawingArea"

Event Draw(View As Image)
Event Scroll

Property Border As Boolean
Property Image As Image
Property Zoom As Float
Property Design As Boolean
Property Grid As Boolean
Property UseMouse As Boolean
Property Alignment As Integer

Property ScrollX As Integer
Property ScrollY As Integer
Property Read ScrollWidth, ScrollW As Integer
Property Read ScrollHeight, ScrollH As Integer

Private $hImage As Image
Private $hZoom As Image
Private $hBorder As Panel
Private $hView As ScrollArea
Private $SX As Integer
Private $SY As Integer

Private $bUseMouse As Boolean = True
Private $fZoom As Float = 1
Private $bZoom As Boolean
Private $bFastZoom As Boolean
Private $bGrid As Boolean
Private $iAlign As Integer = Align.Center

' Private $fBrightness As Float
' Private $fContrast As Float

Public Sub _new()
  
  $hBorder = New Panel(Me)
  $hBorder.Border = Border.Plain
  $hBorder.Arrangement = Arrange.Fill
  
  $hView = New ScrollArea($hBorder) As "View"
  $hView.Border = False
  $hView.Shadow = True
  $hView.Focus = True
  
  Me.Proxy = $hView
  
End


Private Function Image_Read() As Image

  Return $hImage

End

Private Sub Image_Write(Value As Image)

  $hImage = Value
  $fZoom = 1
  UpdateZoom
  View_Arrange

End

Public Sub View_Arrange()
  
  Dim iScrollBar As Integer
  Dim W, H As Integer
  
  If Not $hZoom Then 
    $hView.ResizeContents(1, 1)
    Return
  Endif
  
  W = $hZoom.W
  H = $hZoom.H
  If $bZoom Then
    W *= $fZoom
    H *= $fZoom
  Endif
  
  If W > $hView.W Then iScrollBar += Scroll.Horizontal
  If H > $hView.H Then iScrollBar += Scroll.Vertical
  If $bUseMouse Then $hView.Mouse = If(iScrollBar, Mouse.SizeAll, Mouse.Default)
  $hView.ResizeContents(Max($hView.W, W), Max($hView.H, H))
  $hView.ScrollBar = iScrollBar
  
End

Private Sub GetViewRect() As Rect

  Dim X, Y As Integer
  Dim W As Integer
  Dim H As Integer
  
  If Not $hZoom Then Return
  
  W = $hZoom.W
  H = $hZoom.H
  If $bZoom Then
    W *= $fZoom
    H *= $fZoom
  Endif
  
  If W <= $hView.W Then
    If Align.IsLeft($iAlign) Then
      X = 0
    Else If Align.IsRight($iAlign) Then
      X = $hView.W - W
    Else
      X = ($hView.W - W) \ 2
    Endif
  Else
    X = -$hView.ScrollX
  Endif
  
  If H <= $hView.H Then
    If Align.IsTop($iAlign) Then
      Y = 0
    Else If Align.IsBottom($iAlign) Then
      Y = $hView.H - H
    Else
      Y = ($hView.H - H) \ 2
    Endif
  Else
    Y = -$hView.ScrollY
  Endif
  
  Return Rect(X, Y, W, H)

End

Public Sub ViewToImage(ViewPoint As Point) As Point
  
  Dim hRect As Rect = GetViewRect()
  Dim hResult As Point
  
  If hRect Then
  
    hResult = New Point
    hResult.X = Floor((ViewPoint.X - hRect.X) / $fZoom)
    hResult.Y = Floor((ViewPoint.Y - hRect.Y) / $fZoom)
    Return hResult
    
  Endif
  
End

Public Sub ImageToView(ImagePoint As Point) As Point
  
  Dim hRect As Rect = GetViewRect()
  Dim hResult As Point
  
  If hRect Then

    hResult = New Point  
    hResult.X = hRect.X + ImagePoint.X * $fZoom
    hResult.Y = hRect.Y + ImagePoint.Y * $fZoom
    Return hResult
  
  Endif  
  
End

Public Sub _Paint(hImage As Image, X As Float, Y As Float, Optional hRect As Rect) ', fOpacity As Float = 1.0)
  
  If Not hImage Then Return
  
  If hRect Then 
    X += hRect.X * $fZoom
    Y += hRect.Y * $fZoom
  Else
    hRect = Rect(0, 0, hImage.W, hImage.H)
  Endif
  
  If $bFastZoom Then
    Paint.ZoomImage(hImage, $fZoom, X, Y, If($bGrid, Color.SetAlpha(Color.Black, 128), Color.Default), hRect)
  Else
    Paint.DrawImage(hImage, X, Y, hImage.W * $fZoom, hImage.H * $fZoom,, hRect)
  Endif
  
End

Public Sub View_Draw()
  
  Dim hRect As Rect 
  
  If Not Object.CanRaise(Me, "Draw") Then
    hRect = GetViewRect()
    If hRect Then _Paint($hZoom, hRect.X, hRect.Y)
  Else
    Raise Draw($hZoom)
  Endif
  
End

Private Function Border_Read() As Boolean

  Return $hBorder.Border

End

Private Sub Border_Write(Value As Boolean)

  $hBorder.Border = If(Value, Border.Plain, Border.None)

End

Public Sub View_MouseDown()

  If Not $bUseMouse Then Return
  
  $SX = $hView.ScrollX
  $SY = $hView.ScrollY

End

Public Sub View_MouseMove()

  If Not $bUseMouse Then Return
  
  If Mouse.Left Then $hView.Scroll($SX - (Mouse.X - Mouse.StartX), $SY - (Mouse.Y - Mouse.StartY))

End

Private Function Zoom_Read() As Float

  Return $fZoom

End

Private Sub Zoom_Write(Value As Float)

  Dim X, Y As Float

  Value = Max(0.05, Min(32, Value))
  
  If $fZoom = Value Then Return
  
  X = ($hView.ScrollX + $hView.ClientW / 2) / $fZoom
  Y = ($hView.ScrollY + $hView.ClientH / 2) / $fZoom
  
  $fZoom = Value
  UpdateZoom

  $hView.Scroll(X * $fZoom - $hView.ClientW / 2, Y * $fZoom - $hView.ClientH / 2)

End

Private Sub UpdateZoom()

  $bZoom = False
  $bFastZoom = False
  
  If Not $hImage Then 
    $hZoom = Null
  Else
  
    $hZoom = $hImage
    
    If $fZoom <> 1 Then
      $bZoom = True
      If Int($fZoom) = $fZoom Then $bFastZoom = True
    Endif
    
    ' If $fContrast Or If $fBrightness Then
    '   $hZoom = $hZoom.Copy()
    '   $hZoom.BrightnessContrast($fBrightness, $fContrast)
    ' Endif
  Endif
  
  View_Arrange
  $hView.Refresh

End

Public Sub Update(Optional (Image) As Image)
  
  If {Image} Then $hImage = Image
  UpdateZoom
  
End


Private Function Design_Read() As Boolean

  Return Super.Design

End

Private Sub Design_Write(Value As Boolean)

  If Value Then
    $hImage = Image.Load("stock/32/gambas.png")
    $fZoom = 4
    UpdateZoom
  Endif
  Super.Design = Value

End

Public Sub ZoomFit(Optional Margin As Integer)
  
  If Not $hImage Then Return
  'If IsMissing(Margin) Then Margin = Desktop.Scale
  Zoom_Write(Min(($hView.W - Margin * 2) / $hImage.W, ($hView.H - Margin * 2) / $hImage.H))
  
End


Private Function Grid_Read() As Boolean

  Return $bGrid

End

Private Sub Grid_Write(Value As Boolean)

  If Value = $bGrid Then Return
  $bGrid = Value
  $hView.Refresh

End


Private Function ScrollX_Read() As Integer

  Return $hView.ScrollX

End

Private Sub ScrollX_Write(Value As Integer)

  $hView.ScrollX = Value

End

Private Function ScrollY_Read() As Integer

  Return $hView.ScrollY

End

Private Sub ScrollY_Write(Value As Integer)

  $hView.ScrollY = Value

End

Private Function ScrollWidth_Read() As Integer

  Return $hView.ScrollWidth

End

Private Function ScrollHeight_Read() As Integer

  Return $hView.ScrollHeight

End

Public Sub Scroll(X As Integer, Y As Integer)
  
  $hView.Scroll(X, Y)
  
End

Public Sub View_Scroll()
  
  Raise Scroll
  
End

Private Function UseMouse_Read() As Boolean

  Return $bUseMouse

End

Private Sub UseMouse_Write(Value As Boolean)

  $bUseMouse = Value
  If Not Value Then $hView.Mouse = Mouse.Default

End

Public Sub EnsureVisible(X As Integer, Y As Integer, W As Integer, H As Integer)
  
  $hView.EnsureVisible(X, Y, W, H)
  
End

Private Function Alignment_Read() As Integer

  Return $iAlign

End

Private Sub Alignment_Write(Value As Integer)

  $iAlign = Value
  $hView.Refresh

End
